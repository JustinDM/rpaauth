namespace Economical.RpaAuth.DTOs
{
    public class OutboundUserDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
    }
}