namespace Economical.RpaAuth.DTOs
{
    public class InboundUserDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}