using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Economical.RpaAuth.Models
{
    public class User : IdentityUser<int>
    {
        public virtual ICollection<UserRole> Roles { get; set; }
    }
}