using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Economical.RpaAuth.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Economical.RpaAuth.Data
{
    public class AuthRepository : IAuthRepository
    {
        private readonly DataContext _context;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        public AuthRepository(DataContext context, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public async Task<User> Login(string username, string password)
        {
            var user = await _userManager.FindByNameAsync(username);

            if (user == null)
            {
                return null;
            }

            var result = await _signInManager.CheckPasswordSignInAsync(user, password, false);

            if (result.Succeeded)
            {
                return user;
            }

            return null;
        }

        public async Task<User> Register(string username, string password)
        {
            var user = new User()
            {
                UserName = username,
            };

            var result = await _userManager.CreateAsync(user, password);

            if (result.Succeeded)
            {
                return user;
            }
            
            return null;
        }

        public async Task<bool> UserExists(string username)
        {
            if (await _context.Users.AnyAsync(x => x.UserName == username))
            {
                return true;
            }

            return false;
        }

        private KeyValuePair<byte[], byte[]> CreatePasswordHash(string password)
        {
            KeyValuePair<byte[], byte[]> passwordTuple;

            using(var hmac = new HMACSHA512())
            {
                var hmacSalt = hmac.Key;
                var hmacHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
                
                passwordTuple = new KeyValuePair<byte[], byte[]>(hmacSalt, hmacHash);
            }

            return passwordTuple;
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));

                if(!computedHash.SequenceEqual(passwordHash))
                {
                    return false;
                }

                return true;
            }
        }
    }
}