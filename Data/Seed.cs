using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using Economical.RpaAuth.Models;
using Microsoft.AspNetCore.Identity;

namespace Economical.RpaAuth.Data
{
    public class Seed
    {
        public static void SeedAdminUser(UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            if (!userManager.Users.Any())
            {
                var password = File.ReadAllText("Data/PasswordSeed.txt");

                var roles = new List<Role>()
                {
                    new Role { Name = "Analyst" },
                    new Role { Name = "Bot" },
                    new Role { Name = "Admin" }
                };

                foreach (var i in roles)
                {
                    roleManager.CreateAsync(i).Wait();
                }

                var user = new User()
                {
                    UserName = "AuthAdmin"
                };

                var result = userManager.CreateAsync(user, password).Result;

                if (result.Succeeded)
                {
                    var admin = userManager.FindByNameAsync("AuthAdmin").Result;
                    userManager.AddToRoleAsync(admin, "Admin");
                }
                
            }
        }
    }
}