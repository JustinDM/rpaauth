using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Economical.RpaAuth.Data;
using Economical.RpaAuth.DTOs;
using Economical.RpaAuth.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Economical.RpaAuth.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class AuthController : ControllerBase
    {
        private readonly IAuthRepository _repo;
        private readonly IConfiguration _config;
        private readonly UserManager<User> _userManager;

        public AuthController(IAuthRepository repo, IConfiguration config, UserManager<User> userManager)
        {
            _repo = repo;
            _config = config;
            _userManager = userManager;
        }

        [HttpPost("register")]
        [Authorize(Policy = "RequireAdminRole")]
        // [Authorize(Policy = "RequireAdminRole")]
        public async Task<IActionResult> Register([FromBody] InboundUserDto dto)
        {
            dto.Username = dto.Username.ToLower();
            
            if (await _repo.UserExists(dto.Username))
            {
                return BadRequest("Username already exists");
            }

            var user = await _repo.Register(dto.Username, dto.Password);

            var outboundUser = new OutboundUserDto()
            {
                Id = user.Id,
                Username = user.UserName
            };

            return StatusCode(201, outboundUser);
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] InboundUserDto dto)
        {
            var user = await _repo.Login(dto.Username.ToLower(), dto.Password);

            if (user == null)
            {
                return Unauthorized();
            }


            var outboundUser = new OutboundUserDto()
            {
                Id = user.Id,
                Username = user.UserName
            };
                
            return Ok(new {
                token = GenerateJwtToken(user).Result,
                user = outboundUser
            });
            
        }

        private async Task<string> GenerateJwtToken(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName)
            };

            // injecting usermanager here because it's late and I'm lazy - might not be the best idea
            var roles = await _userManager.GetRolesAsync(user);

            foreach (var i in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, i));
            }

            var key = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value)
            );

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}